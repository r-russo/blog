module.exports = function (eleventyConfig) {
  eleventyConfig.addPassthroughCopy("src/css");
  eleventyConfig.addFilter("dateOnly", function (dateVal, locale = "es-ar") {
    let date = new Date(dateVal);
    return date.toISOString().split("T")[0];
  });
  return {
    dir: {
      input: "src",
      output: "public",
    },
  };
};
